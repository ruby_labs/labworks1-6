# Варіант 10
# Task 1

def f1(x) # 1/4
  2.0 / (1.0 - 4*x)
end

def f2(x) # 0 1
  Math.asin(Math.sqrt(x)) / Math.sqrt(x*(1.0 - x))
end

def prm(a, b, n)
  h = (b.to_f - a.to_f) / n.to_f
  x = a - h/2.0
  int = 0

  n.times {
    x += h
    y = yield(x)
    int += h * y
  }

  int
end

def trp(a, b, n)
  h = (b - a) / n
  x = a
  int = h*0.5*(yield(a) - yield(b))

  loop do
    x += h
    y = yield(x)
    int += h * y

    break if x >= b
  end

  int
end

int_prm1 = prm(-2.2, -1.2, 1000) {|x| f1(x)}
int_trp1 = trp(-2.2, -1.2, 1000) {|x| f1(x)}

int_prm2 = prm(0.2, 0.3, 100) {|x| f2(x)}
int_trp2 = trp(0.2, 0.3, 100) {|x| f2(x)}

print "\nINTEGRALS\n\nFirst function:\n\t1. Prm = #{int_prm1}\n\t2. Trp = #{int_trp1}\n"
print "Second function:\n\t1. Prm = #{int_prm2}\n\t2. Trp = #{int_trp2}\n"

# Task 2

def sum(x, i)
  Math.cos(2.0*i - 1.0)*x / (2.0*i - 1.0)**2
end

def series
  print "\nEnter arg x [0.1..1]: "
  x = gets.chop!.to_f
  while x < 0.1 || x > 1
    print "\tx is out of range. Enter again: "
    x = gets.chop!.to_f
  end

  print "\nEnter n [10..58] or (inf): "
  n = gets.chop!
  cont = true
  inf = false
  while cont
    if n == "inf"
      inf = true
      cont = false
    else
      n = n.to_i
      if n < 10 || n > 58
        print "\tn is out of range. Enter again: "
        n = gets.chop!
      else
        cont = false
      end
    end
  end

  e = 0.001
  sum = 0
  if inf
    i = 0
    loop do
      term = yield(x, i)
      sum += term

      break if term <= e

      i += 1
    end
  else
    (0..n).each { |i|
      term = yield(x, i)
      sum += term
    }
  end

  sum
end

print "\nSERIES\n"
loop do
  sum = series {|x, i| sum(x, i)}
  print "\nSum = #{sum}\n\tAgain? (y) or (n): "
  c = gets.chop!
  break if c == "n"
end

print "Bye!"