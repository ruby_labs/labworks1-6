# Варіант 10

def task1(dots)
  size = dots.length
  sum = 0.0

  (0...size - 1).each { |i|
    sum += (dots[i][0] + dots[i + 1][0]) * (dots[i][1] - dots[i + 1][1])
  }

  sum += (dots[size - 1][0] + dots[0][0]) * (dots[size - 1][1] - dots[0][1])
  sum.abs / 2.0
end

arr = [[50, 162], [62, 123], [71, 92], [82, 73], [112, 58], [159, 40], [217, 34], [264, 31], [199, 29], [354, 29], [402, 27], [435, 32], [476, 77], [489, 97], [503, 140], [506, 181], [508, 219], [497, 243], [483, 248], [419, 256], [382, 228], [370, 199], [362, 151], [342, 125], [288, 121], [257, 160], [248, 211], [239, 247], [207, 267], [181, 273], [119, 271], [101, 250], [87, 217], [105, 188], [81, 179], [62, 176]]
print "Завдання 1. Площа фігури: ", task1(arr)

require 'bigdecimal'

def task2(p, t, r)
  Rational(p ** r * (1 - p ** (-t)))
end

print "\nЗавдання 2. Діапазон рівний ", task2(3, 8, 0)

def task3(number)
  result, it = 0, 0
  loop do
    result += (number % 10) * (2 ** it)
    it += 1
    number /= 10
    break if number.zero?
  end
  result
end

print "\nЗавдання 3. 10101010101010 в десятковій системі: ", task3(10101010101010)

def task4(number)
  result, it = 0, 0
  loop do
    result += (number % 2) * (10 ** it)
    it += 1
    number /= 2
    break if number.zero?
  end
  result
end

print "\nЗавдання 4. 333 в двійковій системі: ", task4(333)