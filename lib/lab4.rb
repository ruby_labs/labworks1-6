require 'matrix'

def task1
  n = 8
  arr_a = Array.new(n).map! { Array.new(n) }
  arr_b = Array.new(n).map! { Array.new(n) }
  vec_x = Array.new(n)
  vec_y = Array.new(n)

  (0...n).each { |i|
    vec_x[i] = rand(10)
    vec_y[i] = rand(10)
    (0...n).each { |j|
      arr_b[i][j] = rand(10)
      if i == j
        arr_a[i][j] = 1
      else
        arr_a[i][j] = rand(10)
      end
    }
  }

  print "A:\n"
  puts arr_a.map(&:inspect)

  print "\nB:\n"
  puts arr_b.map(&:inspect)

  print "\nX:\n"
  puts vec_x.map(&:inspect)

  print "\nY:\n"
  puts vec_y.map(&:inspect)

  @A = Matrix[*arr_a]
  @B = Matrix[*arr_b]
  @X = Vector[*vec_x]
  @Y = Vector[*vec_y]

  c = 1
  while c != 0
    print "\nChoose operation:\n\t(1) num * B\n\t(2) A + B\n\t(3) A transpose\n\t(4) A * B\n\t(5) trace B\n\t(6) scalar X * Y\n\t(7) vec X * Y\n\t(8) norm X\n\t(9) A * X\n\t(10) Y * B\n\t(0) Exit\n"
    print "Enter: "
    c = gets.chop!.to_i

    case c
    when 1
      print "\nEnter num: "
      num = gets.chop!.to_f
      @C = @B * num
      puts @C.to_a.map(&:inspect)
    when 2
      print "\n"
      @C = @A + @B
      puts @C.to_a.map(&:inspect)
    when 3
      print "\n"
      arr_c = arr_a.transpose
      puts arr_c.map(&:inspect)
    when 4
      print "\n"
      @C = @A * @B
      puts @C.to_a.map(&:inspect)
    when 5
      trace = 0
      (0...n).each do |i|
        (0...n).each do |j|
          if i == j
            trace += arr_b[i][j]
          end
        end
      end
      print "\nTrace = ", trace
      print "\n"
    when 6
      product = 0
      (0...n).each do |i|
        product += vec_x[i]*vec_y[i]
      end
      print "\nProduct = ", product
      print "\n"
    when 7
      print "\n"
      vec_v = Array.new(n)
      (0...n).each do |i|
        vec_v[i] = vec_x[i]*vec_y[i]
      end
      puts vec_v.map(&:inspect)
    when 8
      norm = @X.norm
      print "\nNorm = ", norm
      print "\n"
    when 9
      print "\n"
      @AX = @A * @X
      puts @AX.to_a.map(&:inspect)
    when 10
      print "\n"
      vec_yt = Array.new(1).map! { Array.new(n) }
      (0...n).each do |i|
        vec_yt[0][i] = vec_y[i]
      end
      @YB = Matrix[*vec_yt] * @B
      puts @YB.to_a.map(&:inspect)
    else
      print "Closing..."
    end
  end
end

def print_system(arr_a, vec_b)
  (0...@N).each do |i|
    coef = arr_a[i][0]
    if coef == 1
      print "\nx1"
    elsif coef == 0
      print "\n0"
    else
      print "\n#{coef}x1"
    end

    (1...@N).each do |j|
      coef = arr_a[i][j]
      if coef == 1
        print " + x#{j+1}"
      elsif coef == 0
        print " + 0"
      else
        print " + #{arr_a[i][j]}x#{j+1}"
      end
    end
    print " = #{vec_b[i]}"
  end
end

def task2
  print "\nAx = b\nEnter size of matrix A (3..9): "
  @N = gets.chop!.to_i
  while @N > 9 || @N < 3
    print "Must be > 3 and < 9. Enter again: "
    @N = gets.chop!.to_i
  end
  k = rand(9) + 1

  arr_a = Array.new(@N).map! { Array.new(@N) }
  vec_b = Array.new(@N){ |i| i + 1 }

  (0...@N).each do |i|
    (0...@N).each do |j|
      if i == j
        arr_a[i][j] = 2.0
      else
        arr_a[i][j] = k + 2.0
      end
    end
  end

  print "\nSystem:"
  (0...@N).each do |i|
    print "\n#{arr_a[i][0]}x1"
    (1...@N).each do |j|
      print " + #{arr_a[i][j]}x#{j+1}"
    end
    print " = #{vec_b[i]}"
  end

  (0...@N).each do |i|
    a = arr_a[i][i]
    (i...@N).each do |j|
      arr_a[i][j] /= a
    end
    vec_b[i] /= a
    (i+1...@N).each do |ii|
      a = arr_a[ii][i]
      (0...@N).each do |jj|
        arr_a[ii][jj] -= a*arr_a[i][jj]
      end
      vec_b[ii] -= a*vec_b[i]
    end

    print "\n\nstep #{i}"
    print_system(arr_a, vec_b)
  end

  print "\n\nReverse:"
  (1...@N).each do |j|
    (j+1..@N).each do |i|
      a = arr_a[-i][-j]
      if a != 0
        arr_a[-i][-j] -= a*arr_a[-j][-j]
        vec_b[-i] -= a*vec_b[-j]
      end
    end
    print "\n\nstep #{j}"
    print_system(arr_a, vec_b)
  end

  print "\n\nSolution: "
  (0...@N).each do |i|
    print "\nx#{i+1} = #{vec_b[i]}"
  end
end

run = true
while run
  print "\n\nChoose task:\n\t(1) Matrix and vector operations\n\t(2) Gauss\n\t(0) Nothing\nEnter: "
  task = gets.chop!.to_i
  case task
  when 1
    task1
  when 2
    task2
  else
    run = false
    print "\nBye!"
  end
end
