# Варіант 10

class Student
  def initialize(last_name, first_name, dads_name, sex, age, year)
    @last_name = last_name
    @first_name = first_name
    @dads_name = dads_name
    @sex = sex
    @age = age
    @year = year
  end

  attr_accessor :last_name
  attr_accessor :first_name
  attr_accessor :sex
  attr_accessor :age
  attr_accessor :year

  def dads_name
    @dads_name
  end

  def print_student
    print "#{@last_name} #{@first_name} #{@dads_name}, #{sex}, #{age} р., #{year} курс"
  end
end

students = [
  Student.new("Бондар", "Альона", "Віталіївна", "Ж", 19, 3),
  Student.new("Іщенко", "Юрій", "Олександрович", "Ч", 18, 1),
  Student.new("Карпенко", "Мирослав", "Сергійович", "Ч", 20, 4),
  Student.new("Жовтяк", "Олена", "Володимирівна", "Ж", 19, 4),
  Student.new("Гак", "Максим", "Вадимович", "Ч", 17, 1),
  Student.new("Мартинюк", "Святослав", "Ігорович", "Ч", 17, 1),
  Student.new("Собко", "Олена", "Віталіївна", "Ж", 18, 2),
  Student.new("Прокопович", "Юрій", "Олегович", "Ч", 19, 3)
]

def year_by_male_students(students)
  arr = students.select { |student| student.sex == "Ч" }
  arr.group_by(&:year).max_by { |_, student| student.count}[0]
end

def most_popular_names(students)
  male = students.select { |student| student.sex == "Ч" }
  female = students.select { |student| student.sex == "Ж" }

  name_m = male.group_by(&:first_name).max_by { |_, student| student.count}[0]
  name_f = female.group_by(&:first_name).max_by { |_, student| student.count}[0]

  [name_m, name_f]
end

def female_popular_age(students)
  arr = students.select { |student| student.sex == "Ж" }
  age = arr.group_by(&:age).max_by { |_, student| student.count }[0]
  names = arr.select { |student| student.age == age }.map { |student| [student.last_name, student.first_name[0], student.dads_name[0]] }
  res = Array.new
  names.each do |student|
    str = student[0] + " " + student[1] + ". " + student[2] + "."
    res.push(str)
  end
  [age, res.sort]
end

#p female_popular_age(students)

print "Студенти:"
students.each do |student|
  print "\n\t"
  student.print_student
end
print "\n\nКурс з найбільшим відсотком студентів-хлопців: #{year_by_male_students(students)}"

name_m, name_f = most_popular_names(students)
print "\nНайпоширеніше жіноче ім'я - #{name_f}, чоловіче - #{name_m}"

age, arr = female_popular_age(students)
print "\nСписок студенток найпоширенішого віку (#{age} р.):"
arr.each { |student| print "\n\t#{student}" }