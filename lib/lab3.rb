# Варіант 10
# TASK 1

A = true
B = false
C = true
X = 50
Y = 10
Z = -5

def logic1_a(a, b)
  !(a || b) && (a && !b)
end

def logic1_b(a, b, c, y, z)
  (z != y).object_id <= (6 >= y).object_id && a || b && c && x >= 1.5
end

def logic1_c(x, y, z)
  (8 - x * 2 <= z) && (x ** 2 >= y ** 2) || (z >= 15)
end

def logic1_d(x, y, z)
  x > 0 && y < 0 || z >= (x * y - (-y / x)) + (-z) / 2
end

def logic1_e(a, b, c)
  !(a || b && !(c || (!a || b)))
end

def logic1_f(x, y)
  x ** 2 + y ** 2 >= 1 && x >= 0 && y >= 0
end

def logic1_g(a, b, c)
  (a && (c && b != b || a) || c) && b
end

print "TASK 1\n1)\n\ta) ", logic1_a(A, B)
print "\n\tb) ", logic1_b(A, B, C, Y, Z)
print "\n\tc) ", logic1_c(X, Y, Z)
print "\n\td) ", logic1_d(X, Y, Z)
print "\n\te) ", logic1_e(A, B, C)
print "\n\tf) ", logic1_f(X, Y)
print "\n\tg) ", logic1_g(A, B, C)

K = 4.5
P = true

def logic2(x, p)
  !((Math::E ** (x ** 2) - Math.sin(x)) < Math::PI) && (p || !p) # (p || !p) is always true
end

print "\n\n2) ", logic2(K, P)

# TASK 2

def task2_if(x)
  if x > -4 && x <= 0
    (((x - 2).abs / (x ** 2 * Math.cos(x))) ** (1.0 / 7.0)).to_f
  elsif x > 0 && x <= 12
    (1.0 / (Math.tan(x + 1.0 / Math::E ** x) / Math.sin(x) ** 2) ** (2.0 / 7.0)).to_f
  else  # elsif x < -4 || x > 12
    (1.0 / (1.0 + x / (1.0 + x / (1.0 + x)))).to_f
  end
end

def task2_case(x)
  case x
  when -3..0
    return (((x - 2).abs / (x ** 2 * Math.cos(x))) ** (1.0 / 7.0)).to_f
  when 1..12
    return (1.0 / (Math.tan(x + 1.0 / Math::E ** x) / Math.sin(x) ** 2) ** (2.0 / 7.0)).to_f
  else # when x < -4 || x > 12
    return (1.0 / (1.0 + x / (1.0 + x / (1.0 + x)))).to_f
  end
end

print "\n\nTASK 2\nWith if: ", task2_if(-1.0)
print "\nWith case: ", task2_case(-4.0)

# TASK 3
# 1 & 5

def task3_1(x)
  sum, k = 1, -1
  (1..10).each { |i|
    sum += k * (i + 1.0) / (i + 2.0) * x ** i
    k *= -1
  }
  sum
end

def task3_5(n)
  sum = 0
  n.times { sum = Math.sqrt(2 + sum) }
  sum
end

print "\n\nTASK 3\n1) ", task3_1(2)
print "\n5) ", task3_5(5)

# TASK 4

EPS = 10 ** (-5)

def factorial(n)
  n > 1 ? n * factorial(n - 1) : 1.0
end

def inf_sum1
  sum, n = 0.0, 2
  loop do
    term = (factorial(n - 1) / factorial(n + 1)) ** (n * (n + 1))
    sum += term
    n += 1

    break if term.abs <= EPS
  end
  sum
end

def csch(x)
  if x == 0
    print "\nx can`t be 0"
    return
  end
  res, n = 0.0, 1
  loop do
    term = 2.0 * (-1) ** n * (2 ** (2*n) - 1) * n ** (6.0 / 7.0) * x ** (2*n - 1) / factorial(2*n)
    res += term
    n += 1

    break if term.abs <= EPS
  end
  res + 1.0 / x
end

def inf_sum2
  sum, n = 0.0, 1
  loop do
    term = factorial(3*n - 2) * factorial(2*n - 1) / (factorial(4*n) * 5 ** (2*n) * factorial(2*n))
    sum += term
    n += 1

    break if term.abs <= EPS
  end
  sum
end

def ax(a, x)
  res, n = 0.0, 0
  loop do
    term = (x * Math.log(a)) ** n / factorial(n)
    res += term
    n += 1

    break if term.abs <= EPS
  end
  res
end

print "\n\nTASK 4\n1) ", inf_sum1
print "\n2) csch(0.5) = ", 1.0 / Math.sinh(0.5)
print "\n      series = ", csch(0.5)
print "\n   -----------------------------------"
print "\n   16^0.25 = ", 16 ** 0.25
print "\n   16^0.25 = ", ax(16, 0.25)
print "\n3) ", inf_sum2

# TASK 5

N = 10
Cc = 9
PI = 3.141
D1 = (N - 1.0) / (N + Cc - 1.0)
D2 = (PI - PI/N) / (1.5*N + Cc - 1.0)
D3 = (Cc - 2.0) / (2*N - 1.0)

def y(x0)
  (x0 ** (((N - Cc)**2 + 4*N*Cc) / (N**2 - Cc**2))) ** (1.0 / N) + (x0 / N + 12.0) / (6.0 - Cc * x0)
end

def z(x0)
  t = Math.tan(2*PI/9.0 - x0)
  (1.0 - Math.cos(4*x0)) / (Math.cos(2*x0)**(-2) - 1) + (1.0 + Math.cos(4*x0)) / (Math.sin(2*x0)**(-2) - 1) + t**(Cc*1.0/N)
end

print "\n\nTASK 5\n1. Tabulate y :"

(N + Cc).times do |x|
  x0 = 1.0 + x*D1
  y0 = y(x0)
  printf("\n\tx = %2.1f, y = %f", x0, y0)
end

print "\n2. Tabulate z :"

(3*N/2 + Cc).times do |x|
  x0 = PI/N + x*D2
  t = Math.tan(2*PI/9.0 - x0)
  if t > 0
    z0 = z(x0)
    printf("\n\tx = %f, y = %f", x0, z0)
  else
    printf("\n\tx = %f, y is undefined", x0)
  end
end

print "\n3. Tabulate f :"

(2*N).times do |x|
  x0 = 2.0 + x*D3
  t = Math.tan(2*PI/9.0 - x0)
  if x0 > 2 && x0 < N
    f0 = y(x0)
    printf("\n\tx = %f, f = y = %f", x0, f0)
  elsif x > N && x < 2*N
    if t > 0
      f0 = z(x0)
      printf("\n\tx = %f, f = z = %f", x0, f0)
    else
      printf("\n\tx = %f, f = z is undefined", x0)
    end
  else
    if t > 0
      f0 = y(x0) + z(x0)
      printf("\n\tx = %f, f = y+z = %f", x0, f0)
    else
      printf("\n\tx = %f, f = y+z is undefined", x0)
    end
  end
end