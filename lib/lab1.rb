PI = Math::PI
E = Math::E

def task10(x, t, z)
  part1 = E ** (Math.tan(x) ** 2) # = e^(tg^2(x))
  part2_1 = Math.sqrt((t - z).abs) # = √|t - z|
  part2_2 = Math.cos(PI ** 2) ** 3 + (E ** PI) * (z ** 2) # = (cos^3(π^2) + e^π * z^2)
  part3 = 2.3 * 10 ** 1.6 # = 2.3 * 10^1.6

  part1 + part2_1 / part2_2 + part3
end

puts "Дана програма обчислює значення виразу:\n\n"
puts 'y = e^(tg^2(x)) + √|t - z| / (cos^3(π^2) + e^π * z^2) + 2.3 * 10^1.6'

puts 'Введіть значення параметрів'
printf ' x = '
x = gets.chomp
x = x.to_f

printf 't = '
t = gets.chomp
t = t.to_f

printf 'z = '
z = gets.chomp
z = z.to_f

while Math.cos(PI ** 2) ** 3 + (E ** PI) * (z ** 2) == 0.0
  puts 'Ділення на нуль! Оберіть інше значення параметру'
  printf 'z = '
  z = gets.chomp
  z = z.to_f
end

result = task10(x, t, z)

print "\nРезультат обчислень: ", result